from machine_learning import *
from mpl_toolkits import mplot3d
import sys

# 0 : Pclass : 1 or 2 or 3
# 1 : Sex : 1 for male, -1 for female
# 2 : Age : missing data is set to the average age which is approximate 29 years old.
# 3 : SibSp :
# 4 : Parch :
# 5 : Fare :
# 6 : Embarked : C=1 , S=2 , Q=3
# 7 : Survived


# wrapper function for adaline
def adaline_sequence(in_n_inter, in_eta, in_X_train, in_Y_train, in_X_test, in_Y_test):

    # create an adaline and fit the network to training data
    gd = AdalineGD(n_iter=in_n_inter, eta=in_eta).fit(in_X_train, in_Y_train)

    # make a prediction based on the testing inputs
    Y_predict = AdalineGD.predict(gd, in_X_test)

    # count the matching and discrepancy outcomes

    # the number of matching outcomes
    match = 0
    # the number of discrepancies
    discrepancy = 0
    # iterate over the predictions vs real outcomes to compare
    for predict, test in zip(Y_predict, in_Y_test):
        # if the prediction matches the test value, then we increment the match
        if predict == test:
            match = match + 1
        else:
            # else the prediction does not match the predicted value and, then we increment the discrepancies
            discrepancy = discrepancy + 1


    return match / (match+discrepancy) , gd.weights()

def perceptron_sequence(file):
    #############################################################################
    print(50 * '=')
    print('Section: Training a perceptron model')
    print(50 * '-')

    # read in csv file
    df = pd.read_csv(file, header=None)
    # print csv file for visual confirmation
    print(df.tail())

    #############################################################################
    print(50 * '=')
    print('Plotting the data')
    print(50 * '-')

    # extract the X data
    X = df.iloc[:, [0,1]].values

    # extract the Y data
    Y = df.iloc[:, 2].values

    # plot data
    plt.scatter(X[:50, 0], X[:50, 1], color='red', marker='o', label='DataSet 1')
    plt.scatter(X[50:100, 0], X[50:100, 1], color='blue', marker='x', label='DataSet 1')

    plt.title(file)
    plt.xlabel('DataSet 1')
    plt.ylabel('DataSet 2')
    plt.legend(loc='upper left')
    plt.show()

    #############################################################################
    print(50 * '=')
    print('Training the perceptron model')
    print(50 * '-')

    # run the perceptron
    ppn = Perceptron(eta=0.01, n_iter=10).fit(X, Y)

    # plot the errors
    plt.title(file)
    plt.plot(range(1, len(ppn.errors_) + 1), ppn.errors_, marker='o')
    plt.xlabel('Epochs')
    plt.ylabel('Number of misclassifications')
    plt.show()

    #############################################################################
    print(50 * '=')
    print('A function for plotting decision regions')
    print(50 * '-')

    plt.title(file)
    plot_decision_regions(X, Y, classifier=ppn)
    plt.xlabel('DataSet 1')
    plt.ylabel('DataSet 2')
    plt.legend(loc='upper left')
    plt.show()


# Homework 4 - Part 1
perceptron_sequence('converge_simple.csv')

# Homework 4 - Part 2
perceptron_sequence('diverge_simple.csv')

# clear out the csv files
f = open("titanic_results.csv", "w")
f.truncate()
f.close()
f = open("titanic_weights.csv", "w")
f.truncate()
f.close()

# Homework 4 - Part 3 and 4
training_cutoff_rows = 632
data_cutoff = 712

# training_cutoff_rows = 632
# data_cutoff = 890

#############################################################################
print(50 * '=')
print(50 * '-')

# read in csv file
df = pd.read_csv('titanic2.csv', header=None, dtype=np.int64)

# shuffle the data in titanic 2
df = df.sample(frac=1).reset_index(drop=True)

# print for visual confirmation
print(df.tail())

# set the age and fare cost back to float
df.iloc[:,2] = df.iloc[:,2].astype(np.float)
df.iloc[:,5] = df.iloc[:,5].astype(np.float)

# divide by 100 to get the values back to regular values
df.iloc[:, 2] = df.iloc[:, 2] / 100.0
df.iloc[:, 5] = df.iloc[:, 5] / 100.0

# normalize the data
df.iloc[:, 2] = (df.iloc[:, 2] - df.iloc[:, 2].mean()) / df.iloc[:, 2].std()
df.iloc[:, 5] = (df.iloc[:, 5] - df.iloc[:, 5].mean()) / df.iloc[:, 5].std()

# print the end of the data for visual confirmation that everything is in the correct form
print(df.tail())


#############################################################################
print(50 * '=')
print(50 * '-')

# training set on survival
Y_train = df.iloc[0:training_cutoff_rows, -1] #.values
# test set on survival
Y_test = df.iloc[training_cutoff_rows:data_cutoff, -1] #.values

# training set on life factors
X_train = df.iloc[0:training_cutoff_rows, 0:7] #.values
# test set of life factors
X_test = df.iloc[training_cutoff_rows:data_cutoff, 0:7] #.values

# result dataframe
results = pd.DataFrame(columns=['iter', 'learn', 'match'])
_iter = 40
# weights
w = np.zeros(1 + X_train.shape[1])
high_match_value = 0.0
learn = .0001

while 0 < _iter:
    learn = .0001
    while 0 < learn:
        # adaline wrapper function
        m, w = adaline_sequence(in_n_inter=_iter, in_eta=learn, in_X_train=X_train, in_Y_train=Y_train, in_X_test=X_test, in_Y_test=Y_test)
        data = {'iter': [_iter], 'learn': [learn],'match':[m]}
        results = pd.DataFrame.append(results, pd.DataFrame(data))
        learn = learn - .000001

        if m > high_match_value:
            high_match_value = m

    _iter = _iter - 1

# save results to csv
results.to_csv('titanic_results.csv')
pd.DataFrame(w.__abs__()).to_csv('titanic_weights.csv')

print(high_match_value)

# display the figure
fig = plt.figure()
ax = plt.axes(projection="3d")
ax.set_xlabel('X axis - Iterations')
ax.set_ylabel('Y axis - Learning Rate')
ax.set_zlabel('Z axis - Match Probability')
ax.scatter3D(results.iloc[:, 0], results.iloc[:, 1], results.iloc[:, 2])
plt.show()










