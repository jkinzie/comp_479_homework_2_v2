# Code to check if left or right mouse buttons were pressed
import win32api
import time
import csv

# open the csv file
with open('click.csv', 'w',newline='') as csv_output_file:
    csv_output_file.truncate()
    output = csv.writer(csv_output_file, delimiter=',', quotechar=' ', quoting=csv.QUOTE_MINIMAL)
    output.writerow(['First Click Down Time', 'Deadspace Time','Second Click Down Time','Successful Click'])

    # the time the first button was pressed
    first_press = 0.0
    # the time the first button was released
    first_release = 0.0
    # the time the second button was pressed
    second_press = 0.0
    # the time the second button was released
    second_release = 0.0

    # get the current state of the mouse button
    current_state = win32api.GetKeyState(0x01)
    old_state = current_state

    print("Please release the left mouse button!")
    time.sleep(1.00)

    # mode is used to select if we are
    # 1 : in good click mode
    # 2 : in bad click mode
    mode = 1
    # while loop to do good clicks first, then bad clicks second
    while 0 <= mode:

        # the number of iterations to collect clicks
        iterations = 3

        print("")
        print("")
        if 1 == mode:
            print("Make", iterations, "good double clicks")
        else:
            print("Make", iterations, "bad double clicks")
        print("")
        print("")

        # loop to collect clicks
        while 0 < iterations:

            # get current state of the button
            current_state = win32api.GetKeyState(0x01)

            # beginning the first keypress cycle
            if 0 > current_state:

                # get the current time for the first press
                first_press = time.monotonic()

                print("")
                print("")
                print("")
                print("Double Click Cycle Started")

                # update the old state to record the key action
                old_state = current_state

                # wait in the while loop until the current state changes.
                while current_state == old_state:
                    # update the key state to detect change
                    current_state = win32api.GetKeyState(0x01)

                # the first key press is now released and the time is recorded
                first_release = time.monotonic()
                # update the old state to record the key action
                old_state = current_state

                # wait in the while loop until the current state changes.
                # this is waiting for the second key press
                while current_state == old_state:
                    # update the key state to detect change
                    current_state = win32api.GetKeyState(0x01)

                # the second key press is now pressed and the time is recorded
                second_press = time.monotonic()
                # update the old state to record the key action
                old_state = current_state

                # wait in the while loop until the current state changes.
                # this is waiting for the second key release
                while current_state == old_state:
                    # update the key state to detect change
                    current_state = win32api.GetKeyState(0x01)

                # the second key press is now released and the time is recorded
                second_release = time.monotonic()

                print("Double Click Cycle Ended")
                print("")

                # update the old state to record the key action
                old_state = current_state

                # to find the time the first key was pressed,
                # we subtract the time the key was released from the time it started
                first_key_press = first_release - first_press

                # to find the time between the clicks,
                # we subtract the time the second key was pressed form the time the first key was released
                key_hang_time = second_press - first_release

                # to find the time the second key was pressed,
                # we subtract the time the key was released from the time it started
                second_key_press = second_release - second_press

                print("the first keypress time was : ", first_key_press)
                print("the hangtime was : ", key_hang_time)
                print("the second keypress time was : ", second_key_press)

                # output.writerow(['First Click Down Time', 'Deadspace Time', 'Second Click Down Time'])
                output.writerow([first_key_press, key_hang_time, second_key_press, mode])
                iterations = iterations - 1

        # change the mode to 0 for the next iteration to collect bad clicks!
        mode = mode - 1

    # the data collection cycle is concluded
    print("Data Collection Concluded")
    # close the csv file
    csv_output_file.close()





